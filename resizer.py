from PIL import Image, ImageFilter, ExifTags
from math import log, exp
import operator
def imgopen(file, orientation=1):
    img = Image.open(file)
    img = img.convert(mode='RGB')
    if orientation == 0:
        tag = next(x[0] for x in ExifTags.TAGS.items() if x[1] == 'Orientation')
        try:
            exif = img._getexif() or {}
        except:
            exif = {}
        if tag in exif:
            orientation = exif[tag]
        else:
            orientation = 1
    if orientation == 2:
        img = img.transpose(Image.FLIP_LEFT_RIGHT)
    elif orientation == 3:
        img = img.transpose(Image.ROTATE_180)
    elif orientation == 4:
        img = img.transpose(Image.FLIP_TOP_BOTTOM)
    elif orientation == 5:
        img = img.transpose(Image.TRANSPOSE)
    elif orientation == 6:
        img = img.transpose(Image.ROTATE_270)
    elif orientation == 7:
        img = img.transpose(Image.TRANSVERSE)
    elif orientation == 8:
        img = img.transpose(Image.ROTATE_90)
    return img
def box(size, ratio):
    width, height = size
    test = width / height
    scale = exp(abs(log(test / ratio)))
    return tuple(int(round(x)) for x in ((width, height * scale) if test > ratio else (width * scale, height)))
def resize(orig, mat, ratio, radius, out):
    ratio = operator.truediv(*map(float, ratio.split(':')))
    origsize = orig.size
    finalsize = box(origsize, ratio)
    upsclsize = box(finalsize, operator.truediv(*mat.size))
    upscl = mat.resize(upsclsize, resample=Image.LANCZOS)
    upscl = upscl.filter(ImageFilter.GaussianBlur(radius))
    left, upper = (upsclsize[0] - finalsize[0]) // 2, (upsclsize[1] - finalsize[1]) // 2
    right, lower = left + finalsize[0], upper + finalsize[1]
    final = upscl.crop((left, upper, right, lower))
    xoff, yoff = (finalsize[0] - origsize[0]) // 2, (finalsize[1] - origsize[1]) // 2
    final.paste(orig, (xoff, yoff))
    if isinstance(out, str):
        final.save(out)
    else:
        final.save(out, 'jpeg')
if __name__ == '__main__':
    import argparse, re, sys
    parser = argparse.ArgumentParser(description="Change an image's aspect ratio by padding with blurred image content.")
    parser.add_argument('input', type=argparse.FileType('rb'), help="the input file path (or `-` for stdin)")
    parser.add_argument('output', type=argparse.FileType('wb'), help="the output file path (or `-` for stdout)")
    parser.add_argument('ratio', nargs='?', default='1:1', help="the aspect ratio, width:height, desired (default `1:1`)")
    parser.add_argument('-r', '--radius', type=float, default=20, help="radius of the blur kernel (default `20`, `0` off)")
    parser.add_argument('-o', '--orientation', type=int, default=0, help="orientation 1-8, or use metadata (`0`, default)")
    parser.add_argument('-m', '--mat', type=argparse.FileType('rb'), default=None, help="custom pad image file path")
    args = parser.parse_args()
    if not re.match(r'((\d+\.?:)|(\.\d+:)|(\d+\.\d+:))((\d+\.?$)|(\.\d+$)|(\d+\.\d+$))', args.ratio):
        sys.exit("The aspect ratio must be in the form width:height, e.g. 16:9. Width and height are decimal values.")
    if args.radius < 0:
        sys.exit("The radius may not be negative.")
    orig = imgopen(args.input, args.orientation)
    if args.mat:
        mat = imgopen(args.mat)
    else:
        mat = orig
    resize(orig, mat, args.ratio, float(args.radius), args.output)