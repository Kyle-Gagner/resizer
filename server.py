from flask import Flask, Response, render_template, request
from resizer import imgopen, resize
from io import BytesIO
from datetime import datetime

app = Flask(__name__, template_folder='.')

@app.route("/", methods=['GET', 'POST'])
def main():
    if request.method == 'POST':
        infile = request.files['input']
        matfile = request.files['mat']
        orientation = request.values.get('orientation', '1', type=int)
        orig = imgopen(infile, orientation)
        if matfile:
            mat = imgopen(matfile)
        else:
            mat = orig
        ratio = request.values.get('aspect-ratio', '1:1', type=str)
        if ratio == 'custom':
            ratio = request.values.get('custom-aspect-ratio', '1:1', type=str)
        radius = request.values.get('radius', 20, type=float)
        filename = datetime.now().strftime('resized_%Y_%m_%d_%H_%M_%S.jpg')
        with BytesIO() as outfile:
            resize(orig, mat, ratio, radius, outfile)
            return Response(outfile.getvalue(), mimetype='image/jpeg',
                headers={'Content-Disposition': f'attachment; filename="{filename}"'})
    else:
        return render_template('form.html')