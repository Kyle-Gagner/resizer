# Resizer

Change the aspect ratio of an image by padding with blurred image content. Avoids stretching, cropping, and unsightly black bars. Perfect for creating 1:1 aspect ratio content for Instagram without cropping your selfies.

```
python resizer.py selfie.jpg resized.jpg
```

## Examples

|original|default|custom mat|
-|-|-
![original](https://i.imgur.com/M4uDIIe.jpg) | ![original](https://i.imgur.com/k5M8oy9.jpg) | ![original](https://i.imgur.com/nsjgOeC.jpg)

## Requirements

* Python 3
* Pillow

## Usage 

```
usage: resizer.py [-h] [-r RADIUS] [-o ORIENTATION] [-m MAT] input output [ratio]

Change an image's aspect ratio by padding with blurred image content.

positional arguments:
  input                 the input file path (or `-` for stdin)
  output                the output file path (or `-` for stdout)
  ratio                 the aspect ratio, width:height, desired (default `1:1`)

optional arguments:
  -h, --help            show this help message and exit
  -r RADIUS, --radius RADIUS
                        radius of the blur kernel (default `20`, `0` off)
  -o ORIENTATION, --orientation ORIENTATION
                        orientation 1-8, or use metadata (`0`, default)
  -m MAT, --mat MAT     custom pad image file path
```

## Webapp Option

Resizer may be run as a web server in a Docker container.

```
docker build . -t resizer
docker run --rm -p 8080:8080 resizer
```

The environment variable `PORT` is used, or default port 8080.