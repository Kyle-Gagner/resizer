FROM ubuntu
RUN apt update && apt install -y python3 python3-pip
RUN pip3 install Pillow Flask gunicorn
WORKDIR /root/
ADD [ "*.py", "*.html", "./" ]
CMD gunicorn --bind 0.0.0.0:${PORT:-8080} --workers 2 server:app